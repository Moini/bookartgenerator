# Bookart Generator

Some JS / HTML to generate book folding art patterns, used at https://vektorrascheln.de/bookart.html .

Licenses are indicated within each file.